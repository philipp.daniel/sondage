<?php

namespace App\Controller;

use App\Entity\Sondage;
use App\Entity\Question;
use App\Entity\Reponse;
use App\Entity\Feedback;
use App\Form\SondageType;
use App\Repository\SondageRepository;
use App\Controller\GraphController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\Material\ColumnChart;

class SondageController extends AbstractController
{
    private $graphController;

    public function __construct() {
      $this->graphController = new GraphController();
    }
    /**
     * @Route("", name="sondage_index", methods={"GET"})
     */
    public function index(SondageRepository $sondageRepository): Response
    {
        return $this->render('sondage/index.html.twig', [
            'sondages' => $sondageRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="sondage_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $sondage = new Sondage();
        $form = $this->createForm(SondageType::class, $sondage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($sondage);
            $entityManager->flush();

            return $this->redirectToRoute('sondage_index');
        }

        return $this->render('sondage/new.html.twig', [
            'sondage' => $sondage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/feedback/{id}", name="feedback", methods={"GET"})
     */
    public function feedback(Feedback $feedback): Response
    {
      $questions = $feedback->getFkSondage()->getQuestions();
      return $this->render('feedback.html.twig', [
        'questions' => $questions,
        'sondageName' => $feedback->getFkSondage()->getName(),
        'feedback' => $feedback
      ]);
    }

    /**
     * @Route("/insert_feedback/{id}", name="insert_feedback", methods={"POST"})
     */
    public function insertFeedback(Request $request, Feedback $feedback): Response
    {
      $manager = $this->getDoctrine()->getManager();
      foreach ($request->request as $key => $value) {
        $reponse = new Reponse();
        $reponse->setNote($value);
        $reponse->setFkFeedback($feedback);
        $question = $this->getDoctrine()->getRepository(Question::class);
        $reponse->setFkQuestion($question->find($key));
        $manager->persist($reponse);

        $feedback->setStatus(1);
        $manager->persist($feedback);
      }
      $manager->flush();
      return $this->redirectToRoute('sondage_index');
    }

    /**
     * @Route("/{id}/edit", name="sondage_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Sondage $sondage): Response
    {
        $form = $this->createForm(SondageType::class, $sondage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sondage_index');
        }

        return $this->render('sondage/edit.html.twig', [
            'sondage' => $sondage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="sondage_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Sondage $sondage): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sondage->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sondage);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sondage_index');
    }

    /**
     * @Route("/result/{graph}/{id}", name="sondage_result")
     */
    public function result($graph, Sondage $sondage): Response
    {
        $questions = $sondage->getQuestions();
        $chart = self::getGraphData($graph, $questions);
        return $this->render('sondage/result.html.twig', [
            'sondage' => $sondage,
            'charts' => $chart,
            'graph' => $graph
        ]);
    }

    // Prepare data for columnChart and others
    private function getGraphData($graph, $questions){
      $data = [];
      $data1 = [];
      $answers = ["Question", "Pas d'accord", "Plutôt pas d'accord", "Plutôt d'accord", "D'accord"];
      array_push($data1, $answers);

      foreach ($questions as $question) {
        $resp = [];
        foreach ($question->getReponses() as $key => $value) {
          $resp[$key] = $value->getNote();
        }
        $resp = array_count_values($resp);

        // For columnChart and lineChart
        if ($graph != 'pieChart') {
          if (count($resp) != 4) {
            for ($i = 1; $i <= 4; $i++) {
              if (!array_key_exists($i, $resp)) $resp[$i] = 0;
            }
          }
          $resp[0] = $question->getName();
          ksort($resp);
          array_push($data1, $resp);
        } else {
          // For pieChart
          $res[0] = ['Réponse','Combien'];
          ksort($resp);
          for ($i = 1; $i <= 4; $i++) {
            array_push($res, [$answers[$i], array_key_exists($i, $resp) ? $resp[$i] : 0]);
          }
          $chart = $this->graphController->$graph($res, $question->getName());
          $res = [];
          array_push($data, ['res' => $chart, 'questionId' => strval($question->getId())]);
        }
      }
      if ($graph != 'pieChart') array_push($data, ['res' => $this->graphController->$graph($data1), 'questionId' => 'columnChart']);
      return $data;
    }
}
