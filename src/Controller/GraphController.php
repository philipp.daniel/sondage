<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\ColumnChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\LineChart;

class GraphController extends AbstractController
{

  public function columnChart($data)
  {
    $max = 0;
    for ($i = 1; $i < count($data); $i++) {
      for ($t = 1; $t < count($data[$i]); $t++) {
        if ($data[$i][$t] > $max) $max = $data[$i][$t];
      }
    }
    $chart = new ColumnChart();
    $chart->getData()->setArrayToDataTable($data);

    $chart->getOptions()
      ->setColors(['#e4544e', '#b12aa5', '#27d8fe', '#14f1ae'])
      ->setHeight("600");
    $chart->getOptions()->getBar()
      ->setGroupWidth("80%");
    $chart->getOptions()->getChartArea()
      ->setWidth("75%")
      ->setHeight("70%")
      ->setTop(10)
      ->setLeft(30);
    $chart->getOptions()->getHAxis()
      ->setSlantedText(true)
      ->setSlantedTextAngle(90);
    $chart->getOptions()->getVAxis()->getViewWindow()
      ->setMin(0)
      ->setMax($max);
    $chart->getOptions()->getVAxis()
      ->setFormat("0");
    return $chart;
  }

  public function pieChart($data, $questionName)
  {
    $pieChart = new PieChart();
    $pieChart->getData()->setArrayToDataTable($data);
    $pieChart->getOptions()->setTitle($questionName);
    $pieChart->getOptions()->setPieStartAngle(135);
    $pieChart->getOptions()->setHeight(400);
    $pieChart->getOptions()->setColors(['#e4544e', '#b12aa5', '#27d8fe', '#14f1ae']);
    return $pieChart;
  }

  public function lineChart($data)
  {
    $max = 0;
    for ($i = 1; $i < count($data); $i++) {
      for ($t = 1; $t < count($data[$i]); $t++) {
        if ($data[$i][$t] > $max) $max = $data[$i][$t];
      }
    }
    $line = new LineChart();
    $line->getData()->setArrayToDataTable($data);
    $line->getOptions()
      ->setColors(['#e4544e', '#b12aa5', '#27d8fe', '#14f1ae'])
      ->setHeight("400");
    $line->getOptions()->getChartArea()
      ->setWidth("75%")
      ->setHeight("60%")
      ->setTop(10)
      ->setLeft(30);
    $line->getOptions()->setLineWidth(3);
    $line->getOptions()->getHAxis()
      ->setSlantedText(true)
      ->setSlantedTextAngle(90);
    $line->getOptions()->getVAxis()->getViewWindow()
      ->setMin(0)
      ->setMax($max);
    $line->getOptions()->getVAxis()
      ->setFormat("0");
    return $line;
  }
}
