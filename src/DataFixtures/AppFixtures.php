<?php

namespace App\DataFixtures;

use App\Entity\Sondage;
use App\Entity\Question;
use App\Entity\Reponse;
use App\Entity\Feedback;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use Faker\Factory;
use Faker\Generator;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      $faker = Factory::create('fr_FR');

      $sondages = [];
      for( $i = 1; $i <= 4; $i++ ) {
        $sondage = new Sondage();
        $sondage->setName("Sondage de " . $faker->firstname);
        $sondage->setDate($faker->dateTimeBetween('+10 days', '+25 days'));
        $manager->persist($sondage);
        $sondages[$i] = $sondage;
      }

      $questions = [];
      $q = ["Je suis toujours parmi les premiers à acheter le dernier smartphone",
            "Mes proches me demande souvent conseil avant d'acheter un smartphone",
            "Je me tiens au courant de l'actualité des smartphones",
            "J'aime rencontrer de nouvelles personnes", "J'aime aider les gens",
            "Je fais parfois des erreurs", "Je suis souvent déçu",
            "J'aime réparer les choses"];
      $answers = ["D'accord", "Plutôt d'accord", "Plutôt pas d'accord", "Pas d'accord"];
      foreach( $sondages as $sondage ) {
        for( $i = 1; $i <= 8; $i++ ){
            $question = new Question();
            $question->setName($q[$i-1]);
            $question->setFkSondage($sondage);
            $manager->persist($question);
            $questions[$i] = $question;
        }
        for( $i = 1; $i <= 20; $i++ ) {
            $feedback = new Feedback();
            $feedback->setEmail($faker->email);
            $feedback->setFkSondage($sondage);
            $manager->persist($feedback);
            // Change rand parameter to write more or less answers
            if (rand(1,10) > 4) {
              foreach ($questions as $question1) {
                  $reponse = new Reponse();
                  $reponse->setNote(rand(1,4));
                  $reponse->setFkQuestion($question1);
                  $reponse->setFkFeedback($feedback);
                  $manager->persist($reponse);
              }
              $feedback->setStatus(1);
            } else {
              $feedback->setStatus(0);
            }
        }
      }
      $manager->flush();
    }
}
